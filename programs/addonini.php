; <?php/*

[general]
name				="apptester"
version				="0.0.1"
addon_type			="EXTENSION"
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="CRM SIRH"
description.fr		="CRM pour le SIRH"
delete				=1
longdesc			="CRMSIRH"
db_prefix			="apptester_"
ov_version			="8.2.0"
php_version			="5.2.0"
mysql_version		="5.1"
author				="capwelton"

[addons]
widgets				="1.1.82"
LibOrm				="0.9.14"

[functionalities]
jquery				="Available"
Thumbnailer			="Available"

;*/ ?>

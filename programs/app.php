<?php
// namespace Capwelton\AppTester;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2018 by CANTICO ({@link http://www.cantico.fr})
 */

\bab_functionality::includeFile('App');
$addon = bab_getAddonInfosInstance('apptester');

if (!$addon) {
    return;
}

define('APPTESTER_PHP_PATH',    $addon->getPhpPath());
define('APPTESTER_UI_PATH',     APPTESTER_PHP_PATH.'ui/');
define('APPTESTER_SET_PATH',     APPTESTER_PHP_PATH.'set/');

class Func_App_AppTester extends \Func_App
{
    /**
     * @param \Func_App $Crm
     */
    function __construct(\Func_App $App = null)
    {
        parent::__construct($App);
        
        $this->addonPrefix = 'apptester';
        $this->addonName = 'apptester';
        
        $this->classPrefix = $this->addonPrefix . '_';
        $this->controllerTg = 'addon/' . $this->addonName . '/main';
        
        $this->addComponentsFromName('Entry');
        $this->addComponentsFromName('Note');
        $this->addComponentsFromName('Address');
        $this->addComponentsFromName('ContactOrganization');
        $this->addComponentsFromName('Attachment');
        $this->addComponentsFromName('Tag');
        $this->addComponentsFromName('Team');
        $this->addComponentsFromName('Project');
        $this->addComponentsFromName('SalesDocuments');
    }
    
    function includeController()
    {
        parent::includeController();
        require_once dirname(__FILE__) . '/controller.class.php';
    }
    
    function includeUi()
    {
        parent::includeUi();
        require_once APPTESTER_UI_PATH . 'ui.class.php';
    }
    
    function Ui()
    {
        $this->includeUi();
        return new apptester_Ui($this);
    }

    public function CustomSectionSet()
    {
        return app_App()->CustomSectionSet();
    }
    public function CustomFieldSet()
    {
        return app_App()->CustomFieldSet();
    }
    public function CustomContainerSet()
    {
        return app_App()->CustomContainerSet();
    }
    
    /**
     * Includes ProjectSet class definition.
     */
    public function includeProjectSet()
    {
        require_once dirname(__FILE__) . '/set/project.class.php';
    }
    public function ProjectClassName()
    {
        return 'apptester_Project';
    }
    public function ProjectSetClassName()
    {
        return $this->ProjectClassName() . 'Set';
    }
    /**
     * @return apptester_ProjectSet
     */
    public function ProjectSet()
    {
        $this->includeProjectSet();
        $className = $this->ProjectSetClassName();
        $set = new $className($this);
        return $set;
    }
}

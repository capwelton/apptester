<?php
namespace Capwelton\AppTester;

//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
include_once 'base.php';

require_once dirname(__FILE__) . '/vendor/autoload.php';
require_once dirname(__FILE__) . '/functions.php';


$App = apptester_App();

// $timeStart =  microtime(true);


// if (bab_rp('noskin', false) === false) {
// 	bab_skin::applyOnCurrentPage('crmbtp', 'crmbtp.css');
// }

// We use oxygen if it is available.
if ($icons = @\bab_functionality::get('Icons')) {
    $icons->includeCss();
}



// bab_requireCredential(crmsirh_translate('You must be logged in to access this page.'));


// if (bab_rp('clearcrumbs', false)) {
//     crm_BreadCrumbs::clear();
// }

$controller = $App->Controller();


$action = \Widget_Action::fromRequest();


// function crmbtp_shutdown()
// {
//     global $timeStart;

//     $time = microtime(true) - $timeStart;
//     $peak = memory_get_peak_usage();
//     $peakReal = memory_get_peak_usage(true);
// //     echo '<pre>';
// //     echo "Execution time:           " . $time. ' (' . round($time * 1000, 2) . "ms)\n";
// //     echo "Memory peak usage:        " . $peak . ' (' . round($peak / (1024 * 1024), 2) . "MB)\n";
// //     echo "Memory peak usage (real): " . $peakReal . ' (' . round($peakReal / (1024 * 1024), 2) . "MB)\n";
// //     echo '</pre>';
// }

// register_shutdown_function('crmbtp_shutdown');



$controller->execute($action);


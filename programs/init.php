<?php
use Capwelton\AppTester;
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */

require_once dirname(__FILE__) . '/vendor/autoload.php';
/**
 * Fonction appellée au moment de la mise à jour et de l'installation de l'addon
 */
function appTester_upgrade($version_base,$version_ini)
{
    require_once $GLOBALS['babInstallPath'] . 'utilit/upgradeincl.php';
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    
//     $access = new bab_synchronizeSql();
//     $access->setDisplayMessage(false);
//     $access->fromSqlFile(dirname(__FILE__).'/crmbtpresumeaccess.sql');
    
    $addon = bab_getAddonInfosInstance('apptester');
    $addon->removeAllEventListeners();
    
    $phpPath = $addon->getPhpPath();
    
    require_once $phpPath . 'functions.php';
    require_once $phpPath . 'app.php';    
    
    $func = new \bab_functionalities();
    $func->register('App/AppTester', $phpPath . 'app.php');
    
    $App = apptester_App();
    
    
    $mysqlBackend = new ORM_MySqlBackend(bab_getDB());
    $synchronize = new bab_synchronizeSql();
    
    $sets = array(
        $App->CustomSectionSet(),
        $App->CustomFieldSet()
    );
    $sql = "";
    foreach ($sets as $set){
        $sql .= $mysqlBackend->setToSql($set) . "\n";
    }
    
    $synchronize->fromSqlString($sql);
    
    $App->synchronizeComponentsSql('apptester_');
    $App->componentsOnUpdate();
    $App->synchronizeSql('apptester_');
    
    $addon->removeAllEventListeners();
    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'appTester_onSiteMapItems', 'init.php');
    
    appTester_initCountries();
    
    return true;
}


function appTester_initCountries()
{
    $App = apptester_App();
    $set = $App->CountrySet();
    
    if ($set->select()->count() != 0) {
        return;
    }
    
    if ($set->populateFromGeoNames()) {
        bab_installWindow::message(bab_toHtml('Countries database initialization... done'));
    }
}

/**
 * Fonction appellée au moment de la suppression de l'addon
 */
function appTester_onDeleteAddon()
{
    $addon = bab_getAddonInfosInstance('apptester');
    $addon->unregisterFunctionality('App/AppTester');
    $addon->removeAllEventListeners();
    
    return true;
}

/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function appTester_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{
    require_once dirname(__FILE__).'/functions.php';
    
    bab_functionality::includefile('Icons');
    
    $App = apptester_App();
    
    //ROOT
    $item = $event->createItem('apptester_root');
    $item->setLabel($App->translate('Home'));
    $item->setLink($App->Controller()->OrganizationType()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));
    $item->addIconClassname('apps-addon-crmsirh');
    $event->addFolder($item);
    
    //ORGANIZATIONS
    $item = $event->createItem('apptester_organizations');
    $item->setLabel($App->translate('Organizations'));
    $item->setLink($App->Controller()->Organization()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root'));
    $item->addIconClassname(Func_Icons::OBJECTS_ORGANIZATION);
    $event->addFolder($item);
    
    //CONTACTS
    $item = $event->createItem('apptester_contacts');
    $item->setLabel($App->translate('Contacts'));
    $item->setLink($App->Controller()->Contact()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root'));
    $item->addIconClassname(Func_Icons::OBJECTS_CONTACT);
    $event->addFolder($item);
    
    //PROJECT ROOT
    $item = $event->createItem('apptester_projectroot');
    $item->setLabel($App->translate('Projects'));
    $item->setLink($App->Controller()->Project()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root'));
    $item->addIconClassname(Func_Icons::OBJECTS_CONTRACT);
    $event->addFolder($item);
    
    //PROJECTS
    $item = $event->createItem('apptester_projects');
    $item->setLabel($App->translate('Projects'));
    $item->setLink($App->Controller()->Project()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_projectroot'));
    $item->addIconClassname(Func_Icons::OBJECTS_CONTRACT);
    $event->addFolder($item);
    
    //PROJECTS STATUSES
    $item = $event->createItem('apptester_projectstatuses');
    $item->setLabel($App->translate('Project statuses'));
    $item->setLink($App->Controller()->ProjectStatus()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_projectroot'));
    $item->addIconClassname(Func_Icons::OBJECTS_CONTRACT);
    $event->addFolder($item);
    
    //PROJECTS CLASSIFICATIONS
    $item = $event->createItem('apptester_projectclassifications');
    $item->setLabel($App->translate('Project classifications'));
    $item->setLink($App->Controller()->ProjectClassification()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root', 'apptester_projectroot'));
    $item->addIconClassname(Func_Icons::OBJECTS_CONTRACT);
    $event->addFolder($item);
    
    //TEAM TYPE
    $item = $event->createItem('apptester_teamtypes');
    $item->setLabel($App->translate('Team types'));
    $item->setLink($App->Controller()->TeamType()->displayList()->url() . '&clearcrumbs=1');
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root'));
    $item->addIconClassname(Func_Icons::OBJECTS_GROUP);
    $event->addFolder($item);
    
    $item = $event->createItem('apptester_Configuration');
    $item->setLabel($App->translate('Configuration'));
    $item->setLink($App->Controller()->Config()->display()->url());
    $item->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons', 'apptester_root'));
    $item->addIconClassname(Func_Icons::CATEGORIES_PREFERENCES_OTHER);
    $event->addFolder($item);
}
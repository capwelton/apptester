<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2021 by SI4YOU ({@link https://www.siforyou.com/})
 */

require_once dirname(__FILE__) . '/../functions.php';


/**
 * This controller manages the configuration pages.
 *
 * @method Func_App_AppTester App()
 */
class apptester_CtrlConfig extends app_Controller
{

    
    /**
     * Parse the different component in research of a getConfigItems on the definition 
     * @todo: Needs to improve the check for duplicate definition, if Two component came from the meme definition, the definition appear twice in ->getComponents()
     * @return app_Page
     */
    public function display(){
        $App = $this->App();
        $page = $App->Ui()->Page();
        $W = bab_Widgets();
        $configurationFrame = $W->Frame()
        ->setLayout($W->VBoxLayout()->setVerticalSpacing(1, 'em'))
        ->addClass(Func_Icons::ICON_LEFT_48);
        
        $configurationFrame->addItem($W->Title($App->translate('Crm configuration'), 1));
        $components = $App->getComponents();
        $passed = array();
        foreach($components as $component){
            $def = $component->getDefinition();
            //@todo : need to improve this test
            $stringDef = $def->getDefinition();
            if(in_array($stringDef,$passed)){
                continue;
            }
            $passed[] = $stringDef;
            $items = null;
            if(!method_exists($def, "getConfigItems")){
                continue;
            }
            $items = $def->getConfigItems($App);
            $currentSection = $W->Section($stringDef);
            $configurationFrame->addItem($currentSection);
            $currentSection->setFoldable(true);
            $currentSection->addItem($box = $W->FlowItems());
            $box->setHorizontalSpacing(1, 'em')
            ->setVerticalSpacing(1, 'em')
            ->setVerticalAlign('top');
            foreach($items as $content){
                $link = $W->Link(
                    $W->Icon($content["label"] . "\n" . $content["subLabel"], $content["icon"]),
                    $content["action"]
                    );
                $box->addItem($link);
            }
        }
        
        $page->addItem($configurationFrame);
        return $page;
    }
    
}
## AppTester ##

Dans Ovidentia, il est peut-être nécessaire de modifier la méthode getMethodAction du fichier utilit/controller.class.php, en remplacçant "!method_exists" par 
$rc = new ReflectionClass($fullClassName);
if (!$rc->hasMethod($methodName))

Application de test permettant d'importer des [composants compatibles](https://bitbucket.org/account/user/capwelton/projects/APP) avec libApp.

Dans le composer.json, ajouter les composants nécessaires, puis lancer la commande
`composer update && composer dumpautoload -o`

Exemple du fichier composer.json :
```json
{
	"repositories" : [{
			"type" : "vcs",
			"url" : "git@bitbucket.org:capwelton/app_entry.git"
		}
	],
	"autoload" : {
		"psr-4" : {
			"Capwelton\\AppTester\\" : "../programs/"
		}
	},
	"require" : {
		"capwelton/appentry" : "dev-master"
	}
}
```

Dans le répertoire programs/component, ajouter un répertoire ayant le nom du composant (ex : programs/component/entry). Dans ce répertoire, créer les fichiers nécessaires permettant d'implémenter les classes fournies par les composants.

Si le composant contient un set, la class qui l'implémente doit surcharger le constructeur en y ajoutant la commande `$this->setTableName($app->classPrefix.'NOM_DU_COMPOSANT');` (où `NOM_DU_COMPOSANT` est par exemple `Entry`).

Dans le constructeur de App (programs/app.php), ajouter les composants avec les méthodes createComponent() et addComponent()
```php
entryComponent = $this->createComponent(EntrySet::class, EntryCtrl::class, EntryUi::class);
$this->addComponent('Entry', $entryComponent);
```

Lancer ensuite l'installation du module.

